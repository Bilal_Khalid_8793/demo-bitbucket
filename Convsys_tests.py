#importing packages here
import requests
import csv
from bs4 import BeautifulSoup

#request to website url
res= requests.get('https://convsys.net/')
res.text
soup = BeautifulSoup(res.text, 'lxml')    
#scrapping title and a random paragraph
title=soup.title   
para=soup.p

info=[]

#finding all hyperlinks (used to link from one page to another)
for link in soup.find_all('a'):
       info_href = str(link.get(('href')))
       # gives text for the href (link.text)
#       print(link.text)
       info.append(info_href)
       
#printing info to console       
#print(info)

#writing to a CSV file
with open("data.csv", "w") as csvFile:
    writer=csv.writer(csvFile,delimiter="\n")
    writer.writerows([info])
    #above can also be preseted as below in commented
#    writer.writerow(info)
    writer.writerow(title)
    writer.writerow(para)
#gives a specific href.....in given case at index 12    
#print(soup.find_all('a')[12])

# finding divs restraining to a certain class....further restrain to specified href and some paragraphs
#print(soup.find('div',{'class':"iq-ad-block"}).text)
#print(soup.find('div',{'class':"iq-ad-block"}).find('a').text)
#print(soup.find('div',{'class':"iq-ad-block"}).find('p').string)

for divs in soup.findAll('div',{"class":"container"}):
    print(divs.find('p'))