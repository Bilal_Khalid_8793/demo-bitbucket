# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 11:28:35 2020

@author: bilal
"""
import sqlite3 
from employee import Employee

conn = sqlite3.connect('employee.database.db')

cursor=conn.cursor()

# cursor.execute("""Create TABLE Employee (
#                 first text,
#                 last text,
#                 pay integer            
#                 )""")

def insert_emp(emp):
    with conn:
        cursor.execute("Insert Into employee Values (:first,:last, :pay)", 
                {'first':emp.first,'last':emp.last ,'pay':emp.pay})

def get_emps_by_name(firstname):
    cursor.execute("Select * From employee Where first=:first",{'first':firstname})
    return cursor.fetchall()

def update_pay(emp,pay):
    with conn:
        cursor.execute("""UPDATE employee SET pay = :pay
                  WHERE first = :first AND last = :last""",
                  {'first': emp.first, 'last': emp.last, 'pay': pay})

def remove_emp(emp):
    with conn:
        cursor.execute("DELETE from employee WHERE first = :first AND last = :last",
                  {'first': emp.first, 'last': emp.last})


emp_1 = Employee('Luke', 'Skywalker', 100000)
emp_2 = Employee('Han', 'Solo', 950000)
emp_3 = Employee('Obi-Wan', 'Kenobi', 90000)
emp_4 = Employee('Leia', 'Organa', 80000)

# insert_emp(emp_1)
# insert_emp(emp_2)
# insert_emp(emp_3)
# insert_emp(emp_4)

update_pay(emp_4, 75000)
emps = get_emps_by_name('Leia')
print(emps)

# print(emp_4.pay)

# cursor.execute("Insert Into employee Values ('{}', {})".format(emp_1.first,emp_1.pay))
# tuple method
# cursor.execute("Insert Into employee Values (?,?, ?)",(emp_2.first,emp_2.last,emp_2.pay))
# conn.commit()

# dictionary method
# cursor.execute("Insert Into employee Values (:first,:last, :pay)", 
#                 {'first':emp_3.first,'last':emp_3.last ,'pay':emp_3.pay}) 
 
# conn.commit()

# cursor.execute("Select * From employee Where first='Jango'")
# print(cursor.fetchall()) 

# tuple selection output
# cursor.execute("Select * From employee Where first=?",('Vader',))
# print(cursor.fetchall())

# dictionary selection output
# cursor.execute("Select * From employee Where first=:first",{'first':'Jango'})
# print(cursor.fetchall())

# conn.commit()
conn.close()
