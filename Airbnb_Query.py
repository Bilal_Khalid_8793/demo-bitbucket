import requests, sys, bs4, webbrowser
res=requests.get('https://www.google.com/search?q=Airbnb'+ ''.join(sys.argv[1:]))
res.raise_for_status()
soup = bs4.BeautifulSoup(res.text, "html.parser")
linkElements = soup.select('.r,a')
linkToOpen= min(6,len(linkElements))
for i in range(linkToOpen):
    webbrowser.open('https://www.google.com/'+linkElements[i+2].get('href'))